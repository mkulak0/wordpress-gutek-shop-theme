<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php get_header() ?>
<div class="container mx-auto">
    <?php
    if(have_posts()): 
        while(have_posts()): the_post();
            get_template_part('template-parts/content');
        endwhile;
    else:
        echo "<h1>Coś nie działa :(</h1>";
    endif;
    ?>
</div>
<?php get_footer() ?>