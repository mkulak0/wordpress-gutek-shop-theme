<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>


<?php get_header() ?>

<!-- First look with one functional block -->
<div class="py-2 md:py-32 lg:py-64 static shadow-xl" 
        style="background: linear-gradient(90deg, 
                                        <?php echo get_theme_mod('gutek_first_color')?>  0%,
                                        <?php echo get_theme_mod('gutek_second_color')?> 100%);">
    <div class="container mx-auto px-2">
        <div class="text-white text-xl lg:text-2xl 
                    px-4 py-2 
                    flex flex-col custom-padding">

            <?php
            $args = [
                'post_type' => 'functional_block',
                'post_status' => 'publish',
                'post_per_page' => -1,
                'meta_key' => 'type',
                'meta_value' => 'landing_post'
            ];
            $loop = new WP_Query($args);
            while ( $loop->have_posts() ) : $loop->the_post();
                the_content();
            endwhile;
            wp_reset_postdata();
            ?>

        </div>
    </div>
</div>

<!-- Second look with detailes -->
<div class="container mx-auto text-xl py-4 custom-padding">
<?php
    $args = [
                'post_type' => 'functional_block',
                'post_status' => 'publish',
                'post_per_page' => -1,
                'meta_key' => 'type',
                'meta_value' => 'landing_post_detail'
            ];
            $loop2 = new WP_Query($args);
            while ( $loop2->have_posts() ) : $loop2->the_post();
                the_content();
            endwhile;
            wp_reset_postdata();
    ?>
</div>
<!-- Modal z podziękowaniem za newsletter -->
<?php if(isset($_GET["newsletter"]) && $_GET["newsletter"] == 'true'): ?>
    <?php require(get_parent_theme_file_path().'/template-parts/newsletter-modal.php'); ?>
<?php endif; ?>

<?php get_footer() ?>