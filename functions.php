<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php
require get_parent_theme_file_path('/functions-parts/theme-supports.php');
require get_parent_theme_file_path('/functions-parts/functional-blocks.php');
require get_parent_theme_file_path('/functions-parts/sidebars.php');
require get_parent_theme_file_path('/functions-parts/menus.php');
require get_parent_theme_file_path('/functions-parts/contact-form.php');


require get_parent_theme_file_path('/functions-parts/customizer.php');

function gutek_setup(){
    wp_enqueue_style('tailwindcss', get_template_directory_uri().'/tailwind.min.css');
    wp_enqueue_style('style', get_template_directory_uri().'/style.css');

    wp_enqueue_script('modal-script', get_template_directory_uri().'/js/modal.js');
    wp_enqueue_script('mobile-menu-script', get_template_directory_uri().'/js/mobile-menu.js');
}
add_action('wp_enqueue_scripts', 'gutek_setup', 10);



?>