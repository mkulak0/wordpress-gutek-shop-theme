<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div id="mobile-menu" class="fixed z-40 flex justify-center flex-col hidden" style="width:100%; height: 100%; background-color: rgba(0, 0, 0, 0.8);">
    <div class="mobile-menu">
        <button class="menu-item" onclick="closeMenu()">Powrót</button>
    </div>
    <?php wp_nav_menu([
        'menu' => 'header',
        'menu_class' => 'mobile-menu',
        'container' => 'ul',
        'fallback_cb' => false
    ]); ?>
</div>