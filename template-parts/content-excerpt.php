<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="shadow-lg w-full flex p-8 my-2 flex-col md:flex-row" >
    <div class="flex-none">
        <?php the_post_thumbnail('medium'); ?>
    </div>
    <div class="">
        <a href="<?php echo get_permalink() ?>"> <?php the_title('<h1>','</h1>'); ?></a>
        <?php the_excerpt(); ?>
        <a href="<?php echo get_permalink() ?>"><span class="float-right bg-gray-200 hover:bg-gray-100 rounded p-2">Czytaj dalej</span></a>
    </div>
</div>