<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<style>
    .modal {
      transition: opacity 0.25s ease;
    }
    body.modal-active {
      overflow-x: hidden;
      overflow-y: visible !important;
    }
  </style>

<div class="fixed w-full h-full top-0 left-0 flex items-center justify-center z-50" id="modal">
    <div class="absolute w-full h-full bg-gray-900 opacity-50"></div>
    <div class="bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">
      <div class="py-4 text-left px-6">
        <!--Title-->
        <div class="flex justify-between items-center pb-3">
          <p class="text-2xl font-bold">Dziękujemy za zainteresowanie!</p>
        </div>
        <!--Body-->
        <p class="text-lg">
            Właśnie dałeś nam znać, że ludzi, którzy przejmują się rozwojem świadomości na temat komunikacji jest więcej a to napędza nas do działania. Gdy tylko będziemy mieli świeże informacje damy Ci znać. Miłego dnia!
        </p>
        <!--Footer-->
        <div class="flex justify-end pt-2">
          <button class="px-4 p-3 rounded-lg text-white" style="background-color: #85cdca;" onclick="closeModal()">Zamknij</button>
        </div>
        
      </div>
    </div>
    </div>



<script>
    
     
</script>