<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div class="w-full flex p-8 my-2">
    <!-- <div class="flex-none">
        <?php //the_post_thumbnail('medium'); ?>
    </div> -->
    <div class="">
        <?php the_title('<h1>','</h1>'); ?>
        <?php the_content(); ?>
    </div>
</div>