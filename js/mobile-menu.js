function openMenu(){
    let classNames = document.getElementById('mobile-menu').className;
    classNames = classNames.replace(" hidden", "");
    document.getElementById('mobile-menu').className = classNames;
}
function closeMenu(){
    let classNames = document.getElementById('mobile-menu').className;
    classNames += " hidden";
    document.getElementById('mobile-menu').className = classNames;
}