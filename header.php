<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>

<?php
/* PHP for logo */
$custom_logo_id = get_theme_mod( 'custom_logo' );
$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
?>

<body <?php body_class(); ?>>
    <!-- Mobile menu -->
    <?php require(get_parent_theme_file_path().'/template-parts/mobile-menu.php'); ?>
    
    <div class="
        mx-auto flex justify-between items-center
        w-full
        px-4 py-4 
        bg-gray-100
        shadow-2xl
        z-30
        " 
        style="
        position: sticky;
        top: 0;
        border-top: 5px <?php echo get_theme_mod('gutek_first_color')?> solid;
        ">

        <?php if(has_custom_logo()): ?>
            <a href="<?php echo home_url();?>">
                <img src="<?php echo $logo[0] ?>" alt="<?php echo get_bloginfo('name') ?>" style="max-height:80px; width: auto">
            </a>
        <?php else: ?>
            <a href="<?php echo home_url();?>" class="font-hairline text-4xl"><?php echo get_bloginfo('name') ?></a>
        <?php endif;?>

        <!-- Large View Menu -->
        <div class="md:block hidden">
            <?php 
                wp_nav_menu([
                    'menu' => 'header',
                    'menu_class' => 'inline-flex menu-custom-padding',
                    'container' => 'ul',
                    'fallback_cb' => false
                ]);
            ?>
        </div>
        <!-- Mobile Menu -->
        <div class="md:hidden block">
            <button onclick="openMenu()">
                <img src="<?php echo get_bloginfo('template_url') ?>/hamburger.svg">
            </button>
        </div>
    </div>
    
    