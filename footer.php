<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php if(is_active_sidebar('footer_1')): ?>
<div class="container mx-auto shadow-xl px-4 py-4 rounded-t-lg" style="border-top: 5px <?php echo get_theme_mod('gutek_first_color')?> solid; background-color: <?php echo get_theme_mod('gutek_background_color')?>">
<ul id="sidebar">
    <?php dynamic_sidebar('footer_1'); ?>
</ul>
</div>
<?php endif; ?>



<?php wp_footer(); ?>
</body>
</html>
