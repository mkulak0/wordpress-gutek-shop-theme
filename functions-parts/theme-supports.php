<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php
function gutek_theme_supports(){
    //add_theme_support('post-thumbnails');
    add_theme_support('custom-header');
    add_theme_support('custom-logo');
    add_theme_support( 'woocommerce' );
    add_theme_support('automatic-feed-links');
    add_theme_support('customize-selective-refresh-widgets');
}
add_action('after_setup_theme', 'gutek_theme_supports');
?>
