<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php 
$wp_customize->add_section('gutek_colors', [
    'title' => 'Kolorki',
    'panel' => 'gutek_panel',
    'priority' => 10
]);
/* First color */
$wp_customize->add_setting('gutek_first_color', [
    'default' => '#40b3a2'
]);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'gutek_first_color_control', [
    'label' => 'Pierwszy kolor',
    'section' => 'gutek_colors',
    'settings' => 'gutek_first_color'
]));
/* Second color */
$wp_customize->add_setting('gutek_second_color', [
    'default' => '#85cdca'
]);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'gutek_second_color_control', [
    'label' => 'Drugi kolor',
    'section' => 'gutek_colors',
    'settings' => 'gutek_second_color'
]));
/* Backgrounds color */
$wp_customize->add_setting('gutek_background_color', [
    'default' => '#f2fffd'
]);
$wp_customize->add_control(new WP_Customize_Color_Control($wp_customize, 'gutek_background_color_control', [
    'label' => 'Kolor tła dla niektórych okien',
    'section' => 'gutek_colors',
    'settings' => 'gutek_background_color'
]));