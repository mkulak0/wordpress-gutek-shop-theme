<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

add_shortcode('contact-form', 'contact_form');
add_action('admin_post_contact_form',  'handle_contact_form');

function contact_form($atts){
    ob_start();
    ?>
    <div style=""> <!-- max-width: 300px; -->
    <form action="<?php echo esc_url( admin_url('admin-post.php') ); ?>" method="post">
        <input type="hidden" name="action" value="contact_form">
        <div>
            <input type="email" name="email" placeholder="Twój email" 
                class="text-xl bg-gray-100 p-2 rounded-lg m-2"
                style="width: 100%"    
            >
        
        </div>
        <div>
            <textarea name="message" placeholder="Twoja wiadomość" 
                class="text-xl bg-gray-100 p-2 rounded-lg m-2"
                style="height:200px; width: 100%"
                ></textarea>
        </div>


        <input type="submit" value="Prześlij" 
            class="rounded-lg p-2 text-white text-xl my-2 float-right"
            style="background-color: <?php echo get_theme_mod('gutek_first_color')?>">

            <div class="clearfix"></div>
    </form>
    </div>


    <?php
    $ret = ob_get_contents();
    ob_end_clean();

    return $ret;
}

function handle_contact_form(){
    $email = esc_html($_POST['email']);
    $message = esc_html($_POST['message']);

    $our_email = "kontakt@empatify.com";
    $headers = array('charset=UTF-8');

    if($email != '' && filter_var($email, FILTER_VALIDATE_EMAIL)){
        wp_mail($our_email, $email, $message, $headers);
    }
    wp_redirect(home_url());
}