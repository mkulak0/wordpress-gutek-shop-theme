<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php
function gutek_sidebars(){
    register_sidebar([
        'name' => 'Stopka',
        'id' => 'footer_1'
    ]);
}
add_action('widgets_init', 'gutek_sidebars');