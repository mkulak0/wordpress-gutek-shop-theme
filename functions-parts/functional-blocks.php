<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php 
function gutek_functional_block(){
    $labels = array(
        'name'              =>    'Blok funkcjonalny',
        'singular_name'     =>    'Blok funkcjonalny',
        'menu_name'         =>    'Bloki funkcjonalne',
        'name_admin_bar'    =>    'Bloki funkcjonalne',
        'add_new'           =>    'Dodaj nowy',
        'add_new_item'      =>    'Dodaj nowy',
        'new_item'          =>    'Nowy',
        'edit_item'         =>    'Edytuj',
        'view_item'         =>    'Zobacz',
        'all_items'         =>    'Wszystkie',
        'search_items'      =>    'Wyszukaj',
        'parent_item_colon' =>    '???',
        'not_found'          =>   'Nie znaleziono',
        'not_found_in_trash' =>   'Nie znaleziono',
        'archives'          =>    'Archiwa',
        'insert_into_item'  =>    '???',
        'uploaded_to_this_item'=> '???',
        'filter_items_list'  =>   'Filtruj listę',
        'items_list_navigation' =>'Lista nawigacyjna',
        'items_list'       =>     'Lista',
    );
    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'functional_block' ),
        'capability_type'    => 'post',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => ['title', 'editor', 'custom-fields'],//array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments', 'editor'),
        'menu_icon' => 'dashicons-palmtree',
        'show_in_rest' => true,
        'taxonomies' => ['']
    );
 
    register_post_type( 'functional_block', $args );
}
add_action('init', 'gutek_functional_block');

?>