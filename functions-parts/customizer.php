<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<?php
function gutek_theme_customizer($wp_customize){
    $wp_customize->add_panel( 'gutek_panel', [
        'priority'       => 1,
        'theme_supports' => '',
        'title'          => 'Ustawienia do Gutkowego Motywu',
        'description'    => ''
    ]);
    require get_template_directory().'/functions-parts/customizer/custom-colors.php';
}
add_action('customize_register', 'gutek_theme_customizer');
?>
